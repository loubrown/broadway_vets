W<p>e know that finding the best parasite control program for your pets can be daunting and confusing! We offer a range of different products to suit everyone, so contact us today to discuss the most appropriate solution for your pets.</p>

<h3>Fleas</h3>
<p>All dogs and even the cleanest of cats can get fleas! They can pick them up when they are outside and out on walks, or we can carry them into the house on our clothing. Fleas can live and reproduce in our houses all year round thanks to central heating!</p>
<p>Adult fleas live on the animals and their eggs drop off into the environment, developing in our carpets and upholstery. Fleas feed on blood and will bite dogs and cats as well as people. You may see fleas in your dogs or cat’s coat, or you may see flea “dirt”: small red to black granules or dust in the coat. Flea dirt is actually flea faeces. Infection through flea bites causes itching and in some dogs may trigger an allergic reaction in the skin.</p> 
<p>Fleas are the most common cause of skin disease in cats. Most animals will experience mild itching but in some cats infection may cause severe skin irritation with pustules and hair loss. Cats are incredibly efficient groomers, so it may be difficult to see signs of fleas even on an infected cat.</p> 
<p>The most effective way to treat and prevent fleas in dogs and cats is usually with a prescription strength spot-on treatment, although there are other options such as tablets or injections which may be more appropriate in some cases. In general, shop-bought products are not very effective and do not last as long as veterinary products.</p>
<p>If you have treated your animal but are still seeing fleas you may need to use a more effective product and to also treat your house. Flea pupae can persist in carpets and your home environment for many months, regularly re-infecting your animals as they emerge!</p> 
<p>Once the fleas are under control, continue to treat your animals regularly and treat the house regularly to prevent re-infestation. Many flea treatments sold in pet shops and supermarkets can contain substances which are potentially toxic to cats and may not be effective so NEVER use a flea product intended for dogs on your cat as these can sometimes be fatal!</p>
<p>We supply flea products which are safe to use and very effective at killing fleas quickly, as well as effective products for treating your house.</p>


<h3>Mites</h3>
<p>There are several types of mites which can infect dogs at different stages of their life and in different parts of the body.</p>
<p>Mites usually cause itching, irritation, and sometimes secondary bacterial infections, although a few mites are not very itchy and you may notice hair loss or excessive dandruff. Dogs can get mites in their skin, ears, and feet.</p>
<p>Mite infections are also very common in cats, especially ear mites. They can cause itching, irritation, and sometimes secondary bacterial infections.</p>
<p>Most mites can be prevented and treated with a spot-on product, although some mites are better treated with topical treatment, insecticidal shampoos or even oral medication. In general over-the-counter products are not very effective against mites.</p>
<p>Sometimes a mite infection can be a sign of a more serious underlying condition so it is always best to see your vet so that we can diagnose the mites, treat them safely and appropriately, and identify any other disease. If you are concerned about mites in your pets, please contact the surgery to book an appointment.</p>

<h3>Ticks</h3>
<p>Ticks are small spider-like parasites that can transmit infections to both humans and animals by latching on to skin and sucking blood! They are very common in grassy woodland areas but can live in your garden too. They are usually seen in Summer/Autumn but are present all year round.</p>  
<p>Ticks carry diseases such as Lyme disease and Babesiosis so it is very important to have one removed quickly if you find one on yourself or your pet – removing a tick requires a twisting technique and removing one incorrectly can cause infections so it is best not to try it yourself unless you are certain you know how to do it safely!  If you suspect your animal has a tick, contact us to arrange a FREE nurse consultation to have it removed safely.</p> 

<h3>Worms</h3>
<div class="tri-split">
<div>
<h5>Dogs</h5>
<p>The main types of worms that infect dogs are roundworms and tapeworms. In general, adult dogs should be treated for worms every three months, although some dogs may require treating more often depending on their lifestyle.</p>
<p>Worms can cause a variety of signs including diarrhoea, weight loss, coughing, breathing difficulties, and bleeding disorders. If your dog has worms you may see them in their faeces or vomit, or you may not see any at all! Some species of worms can infect children as well, so don’t wait until you see worms to treat your dog!</p>
<p>Lungworm (Angiostrongylus vasorum) infects dogs and can cause a wide range of signs including coughing, lethargy, exercise intolerance, and bleeding abnormalities. Dogs can pick up lungworm from eating an infected slug or snail.</p> 
<p>Be vigilant! Curious dogs, those that eat grass and soil, or chew sticks and toys left outside will be vulnerable to picking up this nasty infection.</p>
<p>In recent years Lungworm has become more prevalent in the UK. It is not currently clear how common it is on the Wirral but we have had cases in the practice and other local vets have seen the disease as well.</p> 
<p>Lungworm can be fatal, so prevention is the safest approach.</p>
<p>There are currently very few products on the market that prevent and treat lungworm. Broadway recommends monthly Advocate spot-on treatments, which will protect your dog from fleas, mites, and all roundworms including lungworm.</p>
</div>
<div>
<h5>Cats</h5>
<p>Cats are very prone to picking up worms as kittens and as adult cats. A cat with a heavy worm burden may develop weakness, diarrhoea and weight loss.</p>
<p>There are two types of worms in cats; Roundworms, which can be contagious to children and cause blindness and Tapeworms, which can be acquired from fleas or through hunting.</p>
<p>Most cats should be wormed every 3 months, but cats that regularly hunt will need treating more often.</p>
<p>There are lots of products that provide effective worming available for cats, from tablets to spot-on preparations. Many products also offer combined protection against worms, fleas, and mites but these may require more frequent applications.</p>
</div>