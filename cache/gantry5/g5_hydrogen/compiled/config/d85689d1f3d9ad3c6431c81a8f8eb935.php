<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledConfig',
    'timestamp' => 1498062620,
    'checksum' => '7507e8dab2d3ca663052d6cc53c01215',
    'files' => [
        'templates/g5_hydrogen/custom/config/9' => [
            'index' => [
                'file' => 'templates/g5_hydrogen/custom/config/9/index.yaml',
                'modified' => 1498062615
            ],
            'index2' => [
                'file' => 'templates/g5_hydrogen/custom/config/9/index2.yaml',
                'modified' => 1498043366
            ],
            'layout' => [
                'file' => 'templates/g5_hydrogen/custom/config/9/layout.yaml',
                'modified' => 1498062574
            ],
            'layout2' => [
                'file' => 'templates/g5_hydrogen/custom/config/9/layout2.yaml',
                'modified' => 1498043366
            ],
            'page/assets' => [
                'file' => 'templates/g5_hydrogen/custom/config/9/page/assets.yaml',
                'modified' => 1498043367
            ],
            'page/head' => [
                'file' => 'templates/g5_hydrogen/custom/config/9/page/head.yaml',
                'modified' => 1498043367
            ]
        ],
        'templates/g5_hydrogen/custom/config/default' => [
            'index' => [
                'file' => 'templates/g5_hydrogen/custom/config/default/index.yaml',
                'modified' => 1498043368
            ],
            'layout' => [
                'file' => 'templates/g5_hydrogen/custom/config/default/layout.yaml',
                'modified' => 1498043368
            ],
            'page/assets' => [
                'file' => 'templates/g5_hydrogen/custom/config/default/page/assets.yaml',
                'modified' => 1498043368
            ],
            'page/body' => [
                'file' => 'templates/g5_hydrogen/custom/config/default/page/body.yaml',
                'modified' => 1498043368
            ],
            'page/head' => [
                'file' => 'templates/g5_hydrogen/custom/config/default/page/head.yaml',
                'modified' => 1498043368
            ],
            'particles/branding' => [
                'file' => 'templates/g5_hydrogen/custom/config/default/particles/branding.yaml',
                'modified' => 1498043370
            ],
            'particles/content' => [
                'file' => 'templates/g5_hydrogen/custom/config/default/particles/content.yaml',
                'modified' => 1498043370
            ],
            'particles/contentarray' => [
                'file' => 'templates/g5_hydrogen/custom/config/default/particles/contentarray.yaml',
                'modified' => 1498043370
            ],
            'particles/copyright' => [
                'file' => 'templates/g5_hydrogen/custom/config/default/particles/copyright.yaml',
                'modified' => 1498043370
            ],
            'particles/custom' => [
                'file' => 'templates/g5_hydrogen/custom/config/default/particles/custom.yaml',
                'modified' => 1498043369
            ],
            'particles/date' => [
                'file' => 'templates/g5_hydrogen/custom/config/default/particles/date.yaml',
                'modified' => 1498043369
            ],
            'particles/logo' => [
                'file' => 'templates/g5_hydrogen/custom/config/default/particles/logo.yaml',
                'modified' => 1498043369
            ],
            'particles/menu' => [
                'file' => 'templates/g5_hydrogen/custom/config/default/particles/menu.yaml',
                'modified' => 1498043369
            ],
            'particles/messages' => [
                'file' => 'templates/g5_hydrogen/custom/config/default/particles/messages.yaml',
                'modified' => 1498043369
            ],
            'particles/mobile-menu' => [
                'file' => 'templates/g5_hydrogen/custom/config/default/particles/mobile-menu.yaml',
                'modified' => 1498043369
            ],
            'particles/module' => [
                'file' => 'templates/g5_hydrogen/custom/config/default/particles/module.yaml',
                'modified' => 1498043369
            ],
            'particles/position' => [
                'file' => 'templates/g5_hydrogen/custom/config/default/particles/position.yaml',
                'modified' => 1498043369
            ],
            'particles/sample' => [
                'file' => 'templates/g5_hydrogen/custom/config/default/particles/sample.yaml',
                'modified' => 1498043369
            ],
            'particles/social' => [
                'file' => 'templates/g5_hydrogen/custom/config/default/particles/social.yaml',
                'modified' => 1498043369
            ],
            'particles/spacer' => [
                'file' => 'templates/g5_hydrogen/custom/config/default/particles/spacer.yaml',
                'modified' => 1498043369
            ],
            'particles/totop' => [
                'file' => 'templates/g5_hydrogen/custom/config/default/particles/totop.yaml',
                'modified' => 1498043368
            ],
            'styles' => [
                'file' => 'templates/g5_hydrogen/custom/config/default/styles.yaml',
                'modified' => 1498043368
            ]
        ],
        'templates/g5_hydrogen/config/default' => [
            'particles/logo' => [
                'file' => 'templates/g5_hydrogen/config/default/particles/logo.yaml',
                'modified' => 1498043366
            ]
        ]
    ],
    'data' => [
        'particles' => [
            'clientquotes' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true
            ],
            'newsletter' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true
            ],
            'sample' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '0'
            ],
            'branding' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '0',
                'content' => 'Powered by <a href="http://www.gantry.org/" title="Gantry Framework" class="g-powered-by">Gantry Framework</a>',
                'css' => [
                    'class' => 'branding'
                ]
            ],
            'copyright' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'date' => [
                    'start' => 'now',
                    'end' => 'now'
                ],
                'owner' => ''
            ],
            'custom' => [
                'caching' => [
                    'type' => 'config_matches',
                    'values' => [
                        'twig' => '0',
                        'filter' => '0'
                    ]
                ],
                'enabled' => '0',
                'twig' => '0',
                'filter' => '0'
            ],
            'logo' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '0',
                'link' => '1',
                'url' => '',
                'image' => 'gantry-assets://images/gantry5-logo.png',
                'text' => 'Gantry 5',
                'class' => 'gantry-logo',
                'svg' => ''
            ],
            'menu' => [
                'caching' => [
                    'type' => 'menu'
                ],
                'enabled' => '0',
                'menu' => '',
                'base' => '/',
                'startLevel' => '1',
                'maxLevels' => '0',
                'renderTitles' => '0',
                'hoverExpand' => '1',
                'mobileTarget' => '0'
            ],
            'mobile-menu' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '0'
            ],
            'social' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '0',
                'css' => [
                    'class' => 'social'
                ],
                'target' => '_blank',
                'display' => 'both',
                'title' => '',
                'items' => [
                    
                ]
            ],
            'spacer' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1'
            ],
            'totop' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'css' => [
                    'class' => 'totop'
                ],
                'icon' => '',
                'content' => ''
            ],
            'analytics' => [
                'enabled' => true,
                'ua' => [
                    'anonym' => false,
                    'ssl' => false,
                    'debug' => false
                ]
            ],
            'assets' => [
                'enabled' => true
            ],
            'content' => [
                'enabled' => '1'
            ],
            'contentarray' => [
                'enabled' => '1',
                'article' => [
                    'filter' => [
                        'featured' => ''
                    ],
                    'limit' => [
                        'total' => 2,
                        'columns' => 2,
                        'start' => 0
                    ],
                    'sort' => [
                        'orderby' => 'publish_up',
                        'ordering' => 'ASC'
                    ],
                    'display' => [
                        'image' => [
                            'enabled' => 'intro'
                        ],
                        'text' => [
                            'type' => 'intro',
                            'limit' => '',
                            'formatting' => 'text'
                        ],
                        'title' => [
                            'enabled' => '',
                            'limit' => ''
                        ],
                        'date' => [
                            'enabled' => '',
                            'format' => 'l, F d, Y'
                        ],
                        'read_more' => [
                            'enabled' => 'show',
                            'label' => '',
                            'css' => ''
                        ],
                        'author' => [
                            'enabled' => 'show'
                        ],
                        'category' => [
                            'enabled' => 'link'
                        ],
                        'hits' => [
                            'enabled' => 'show'
                        ]
                    ]
                ],
                'css' => [
                    'class' => ''
                ],
                'extra' => [
                    
                ]
            ],
            'date' => [
                'enabled' => '0',
                'css' => [
                    'class' => 'date'
                ],
                'date' => [
                    'formats' => 'l, F d, Y'
                ]
            ],
            'frameworks' => [
                'enabled' => true,
                'jquery' => [
                    'enabled' => 0,
                    'ui_core' => 0,
                    'ui_sortable' => 0
                ],
                'bootstrap' => [
                    'enabled' => 0
                ],
                'mootools' => [
                    'enabled' => 0,
                    'more' => 0
                ]
            ],
            'lightcase' => [
                'enabled' => true
            ],
            'messages' => [
                'enabled' => '1'
            ],
            'module' => [
                'enabled' => '1',
                'chrome' => ''
            ],
            'position' => [
                'enabled' => '1',
                'chrome' => ''
            ]
        ],
        'page' => [
            'doctype' => 'html',
            'body' => [
                'class' => 'gantry',
                'attribs' => [
                    'class' => 'gantry',
                    'id' => '',
                    'extra' => [
                        
                    ]
                ],
                'layout' => [
                    'sections' => '0'
                ],
                'body_top' => '',
                'body_bottom' => ''
            ],
            'assets' => [
                'favicon' => '',
                'touchicon' => '',
                'css' => [
                    
                ],
                'javascript' => [
                    0 => [
                        'location' => '/bv/custom.js',
                        'inline' => '',
                        'in_footer' => '0',
                        'extra' => [
                            
                        ],
                        'priority' => '0',
                        'name' => 'Custom JS'
                    ]
                ]
            ],
            'head' => [
                'meta' => [
                    
                ],
                'head_bottom' => '<meta name="viewport" content="width=1200px">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=1921998558057635";
  fjs.parentNode.insertBefore(js, fjs);
}(document, \'script\', \'facebook-jssdk\'));</script>',
                'atoms' => [
                    
                ]
            ]
        ],
        'styles' => [
            'accent' => [
                'color-1' => '#439a86',
                'color-2' => '#8f4dae'
            ],
            'base' => [
                'background' => '#ffffff',
                'text-color' => '#666666',
                'body-font' => 'roboto, sans-serif',
                'heading-font' => 'roboto, sans-serif'
            ],
            'breakpoints' => [
                'large-desktop-container' => '75rem',
                'desktop-container' => '60rem',
                'tablet-container' => '48rem',
                'large-mobile-container' => '30rem',
                'mobile-menu-breakpoint' => '48rem'
            ],
            'feature' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'footer' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'header' => [
                'background' => '#2a816d',
                'text-color' => '#ffffff'
            ],
            'main' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'menu' => [
                'col-width' => '180px',
                'animation' => 'g-fade'
            ],
            'navigation' => [
                'background' => '#439a86',
                'text-color' => '#ffffff',
                'overlay' => 'rgba(0, 0, 0, 0.4)'
            ],
            'offcanvas' => [
                'background' => '#354d59',
                'text-color' => '#ffffff',
                'width' => '17rem',
                'toggle-color' => '#ffffff',
                'toggle-visibility' => '1'
            ],
            'showcase' => [
                'background' => '#354d59',
                'image' => '',
                'text-color' => '#ffffff'
            ],
            'subfeature' => [
                'background' => '#f0f0f0',
                'text-color' => '#666666'
            ],
            'preset' => 'preset1'
        ],
        'index' => [
            'name' => '9',
            'timestamp' => 1497276823,
            'version' => 7,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/3-col.png',
                'name' => '_joomla_-_gantry4',
                'timestamp' => 1496913573
            ],
            'positions' => [
                'top' => 'Top',
                'header' => 'Header',
                'slip' => 'Slip',
                'resp-menu' => 'Responsive Menu',
                'slideshow' => 'Slideshow',
                'gallery' => 'Gallery',
                'mainbottom' => 'Mainbottom',
                'extension' => 'Extension',
                'bottom' => 'Bottom',
                'lowest' => 'Lowest',
                'footer' => 'Footer',
                'copyright' => 'Copyright'
            ],
            'sections' => [
                'drawer' => 'Drawer',
                'top' => 'Top',
                'navigation' => 'Navigation',
                'slip' => 'Slip',
                'showcase' => 'Showcase',
                'feature' => 'Feature',
                'utility' => 'Utility',
                'breadcrumb' => 'Breadcrumb',
                'maintop' => 'Maintop',
                'gallery' => 'Gallery',
                'mainbottom' => 'Mainbottom',
                'extension' => 'Extension',
                'bottom' => 'Bottom',
                'lowest' => 'Lowest',
                'copyright' => 'Copyright',
                'header' => 'Header',
                'main-mainbody' => 'Mainbody',
                'sidebar' => 'Sidebar',
                'footer' => 'Footer',
                'offcanvas' => 'Offcanvas'
            ],
            'particles' => [
                'position' => [
                    'position-position-4033' => 'Top',
                    'position-position-4245' => 'Header',
                    'position-position-6311' => 'Slip',
                    'position-position-9419' => 'Responsive Menu',
                    'position-position-7473' => 'Slideshow',
                    'position-position-5350' => 'Gallery',
                    'position-position-1068' => 'Mainbottom',
                    'position-position-9973' => 'Extension',
                    'position-position-8707' => 'Bottom',
                    'position-position-5886' => 'Lowest',
                    'position-footer' => 'Footer',
                    'position-position-2961' => 'Copyright'
                ],
                'content' => [
                    'system-content-6237' => 'Page Content'
                ]
            ],
            'inherit' => [
                
            ]
        ],
        'layout' => [
            'version' => 2,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/3-col.png',
                'name' => '_joomla_-_gantry4',
                'timestamp' => 1496913573
            ],
            'layout' => [
                '/drawer/' => [
                    
                ],
                '/top/' => [
                    0 => [
                        0 => 'position-position-4033'
                    ]
                ],
                '/header/' => [
                    0 => [
                        0 => 'position-position-4245'
                    ]
                ],
                '/navigation/' => [
                    0 => [
                        0 => 'position-position-9419'
                    ]
                ],
                '/slip/' => [
                    0 => [
                        0 => 'position-position-6311'
                    ]
                ],
                '/showcase/' => [
                    0 => [
                        0 => 'position-position-7473'
                    ]
                ],
                '/feature/' => [
                    
                ],
                '/utility/' => [
                    
                ],
                '/breadcrumb/' => [
                    
                ],
                '/maintop/' => [
                    
                ],
                '/container-main/' => [
                    0 => [
                        0 => [
                            'main-mainbody 80' => [
                                0 => [
                                    0 => 'system-content-6237'
                                ]
                            ]
                        ],
                        1 => [
                            'sidebar 20' => [
                                
                            ]
                        ]
                    ]
                ],
                '/gallery/' => [
                    0 => [
                        0 => 'position-position-5350'
                    ]
                ],
                '/mainbottom/' => [
                    0 => [
                        0 => 'position-position-1068'
                    ]
                ],
                '/extension/' => [
                    0 => [
                        0 => 'position-position-9973'
                    ]
                ],
                '/bottom/' => [
                    0 => [
                        0 => 'position-position-8707'
                    ]
                ],
                '/lowest/' => [
                    0 => [
                        0 => 'position-position-5886'
                    ]
                ],
                '/footer/' => [
                    0 => [
                        0 => 'position-footer'
                    ]
                ],
                '/copyright/' => [
                    0 => [
                        0 => 'position-position-2961'
                    ]
                ],
                'offcanvas' => [
                    
                ]
            ],
            'structure' => [
                'drawer' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'top' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'header' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'navigation' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'slip' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'showcase' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'feature' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'utility' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'breadcrumb' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'maintop' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'main-mainbody' => [
                    'title' => 'Mainbody'
                ],
                'sidebar' => [
                    'type' => 'section',
                    'subtype' => 'aside',
                    'block' => [
                        'fixed' => 1
                    ]
                ],
                'container-main' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'gallery' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'mainbottom' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'extension' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'bottom' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'lowest' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'footer' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'copyright' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ]
            ],
            'content' => [
                'position-position-4033' => [
                    'title' => 'Top',
                    'attributes' => [
                        'key' => 'top'
                    ],
                    'block' => [
                        'class' => 'top'
                    ]
                ],
                'position-position-4245' => [
                    'title' => 'Header',
                    'attributes' => [
                        'key' => 'header'
                    ],
                    'block' => [
                        'class' => 'header'
                    ]
                ],
                'position-position-6311' => [
                    'title' => 'Slip',
                    'attributes' => [
                        'key' => 'slip'
                    ],
                    'block' => [
                        'class' => 'slip'
                    ]
                ],
                'position-position-9419' => [
                    'title' => 'Responsive Menu',
                    'attributes' => [
                        'key' => 'resp-menu'
                    ],
                    'block' => [
                        'class' => 'responsive-menu'
                    ]
                ],
                'position-position-7473' => [
                    'title' => 'Slideshow',
                    'attributes' => [
                        'key' => 'slideshow'
                    ],
                    'block' => [
                        'class' => 'slideshow'
                    ]
                ],
                'position-position-5350' => [
                    'title' => 'Gallery',
                    'attributes' => [
                        'key' => 'gallery'
                    ],
                    'block' => [
                        'class' => 'gallery'
                    ]
                ],
                'position-position-1068' => [
                    'title' => 'Mainbottom',
                    'attributes' => [
                        'key' => 'mainbottom'
                    ],
                    'block' => [
                        'class' => 'main-bottom mainbottom'
                    ]
                ],
                'position-position-9973' => [
                    'title' => 'Extension',
                    'attributes' => [
                        'key' => 'extension'
                    ],
                    'block' => [
                        'class' => 'extension'
                    ]
                ],
                'position-position-8707' => [
                    'title' => 'Bottom',
                    'attributes' => [
                        'key' => 'bottom'
                    ],
                    'block' => [
                        'class' => 'bottom'
                    ]
                ],
                'position-position-5886' => [
                    'title' => 'Lowest',
                    'attributes' => [
                        'key' => 'lowest'
                    ],
                    'block' => [
                        'class' => 'lowest'
                    ]
                ],
                'position-footer' => [
                    'attributes' => [
                        'key' => 'footer'
                    ]
                ],
                'position-position-2961' => [
                    'title' => 'Copyright',
                    'attributes' => [
                        'key' => 'copyright'
                    ],
                    'block' => [
                        'class' => 'copy'
                    ]
                ]
            ]
        ],
        'index2' => [
            'name' => 9,
            'timestamp' => 1496936265,
            'version' => 7,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'default',
                'timestamp' => 1496913581
            ],
            'positions' => [
                'header' => 'Header',
                'breadcrumbs' => 'Breadcrumbs',
                'resp-menu' => 'Responsive Menu',
                'footer' => 'Footer'
            ],
            'sections' => [
                'header' => 'Header',
                'navigation' => 'Navigation',
                'main' => 'Main',
                'footer' => 'Footer',
                'offcanvas' => 'Offcanvas'
            ],
            'particles' => [
                'logo' => [
                    'logo-8176' => 'Logo'
                ],
                'position' => [
                    'position-header' => 'Header',
                    'position-breadcrumbs' => 'Breadcrumbs',
                    'position-position-3999' => 'Responsive Menu',
                    'position-footer' => 'Footer'
                ],
                'messages' => [
                    'system-messages-5941' => 'System Messages'
                ],
                'content' => [
                    'system-content-9260' => 'Page Content'
                ]
            ],
            'inherit' => [
                'default' => [
                    'navigation' => 'navigation',
                    'footer' => 'footer',
                    'offcanvas' => 'offcanvas',
                    'position-position-3999' => 'position-position-9419',
                    'position-footer' => 'position-footer'
                ]
            ]
        ],
        'layout2' => [
            'version' => 2,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'default',
                'timestamp' => 1496913581
            ],
            'layout' => [
                '/header/' => [
                    0 => [
                        0 => 'logo-8176 30',
                        1 => 'position-header 70'
                    ]
                ],
                'navigation' => [
                    
                ],
                '/main/' => [
                    0 => [
                        0 => 'position-breadcrumbs'
                    ],
                    1 => [
                        0 => 'system-messages-5941'
                    ],
                    2 => [
                        0 => 'system-content-9260'
                    ]
                ],
                'footer' => [
                    
                ],
                'offcanvas' => [
                    
                ]
            ],
            'structure' => [
                'header' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'navigation' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ]
                ],
                'main' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'footer' => [
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ]
                ],
                'offcanvas' => [
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ]
                ]
            ],
            'content' => [
                'position-header' => [
                    'attributes' => [
                        'key' => 'header'
                    ]
                ],
                'position-breadcrumbs' => [
                    'attributes' => [
                        'key' => 'breadcrumbs'
                    ]
                ]
            ]
        ]
    ]
];
