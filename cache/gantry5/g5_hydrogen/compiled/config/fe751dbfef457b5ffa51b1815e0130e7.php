<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledConfig',
    'timestamp' => 1498062489,
    'checksum' => '339bb56b4743c4f9b5b8f4fc0781c902',
    'files' => [
        'templates/g5_hydrogen/custom/config/9' => [
            'index' => [
                'file' => 'templates/g5_hydrogen/custom/config/9/index.yaml',
                'modified' => 1498043366
            ],
            'index2' => [
                'file' => 'templates/g5_hydrogen/custom/config/9/index2.yaml',
                'modified' => 1498043366
            ],
            'layout' => [
                'file' => 'templates/g5_hydrogen/custom/config/9/layout.yaml',
                'modified' => 1498043366
            ],
            'layout2' => [
                'file' => 'templates/g5_hydrogen/custom/config/9/layout2.yaml',
                'modified' => 1498043366
            ],
            'page/assets' => [
                'file' => 'templates/g5_hydrogen/custom/config/9/page/assets.yaml',
                'modified' => 1498043367
            ],
            'page/head' => [
                'file' => 'templates/g5_hydrogen/custom/config/9/page/head.yaml',
                'modified' => 1498043367
            ]
        ]
    ],
    'data' => [
        'index' => [
            'name' => '9',
            'timestamp' => 1497276823,
            'version' => 7,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/3-col.png',
                'name' => '_joomla_-_gantry4',
                'timestamp' => 1496913573
            ],
            'positions' => [
                'top' => 'Top',
                'header' => 'Header',
                'slip' => 'Slip',
                'resp-menu' => 'Responsive Menu',
                'slideshow' => 'Slideshow',
                'gallery' => 'Gallery',
                'mainbottom' => 'Mainbottom',
                'extension' => 'Extension',
                'bottom' => 'Bottom',
                'lowest' => 'Lowest',
                'footer' => 'Footer',
                'copyright' => 'Copyright'
            ],
            'sections' => [
                'drawer' => 'Drawer',
                'top' => 'Top',
                'slip' => 'Slip',
                'navigation' => 'Navigation',
                'showcase' => 'Showcase',
                'feature' => 'Feature',
                'utility' => 'Utility',
                'breadcrumb' => 'Breadcrumb',
                'maintop' => 'Maintop',
                'gallery' => 'Gallery',
                'mainbottom' => 'Mainbottom',
                'extension' => 'Extension',
                'bottom' => 'Bottom',
                'lowest' => 'Lowest',
                'copyright' => 'Copyright',
                'header' => 'Header',
                'main-mainbody' => 'Mainbody',
                'sidebar' => 'Sidebar',
                'footer' => 'Footer',
                'offcanvas' => 'Offcanvas'
            ],
            'particles' => [
                'position' => [
                    'position-position-4033' => 'Top',
                    'position-position-4245' => 'Header',
                    'position-position-6311' => 'Slip',
                    'position-position-9419' => 'Responsive Menu',
                    'position-position-7473' => 'Slideshow',
                    'position-position-5350' => 'Gallery',
                    'position-position-1068' => 'Mainbottom',
                    'position-position-9973' => 'Extension',
                    'position-position-8707' => 'Bottom',
                    'position-position-5886' => 'Lowest',
                    'position-footer' => 'Footer',
                    'position-position-2961' => 'Copyright'
                ],
                'content' => [
                    'system-content-6237' => 'Page Content'
                ]
            ],
            'inherit' => [
                
            ]
        ],
        'index2' => [
            'name' => 9,
            'timestamp' => 1496936265,
            'version' => 7,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'default',
                'timestamp' => 1496913581
            ],
            'positions' => [
                'header' => 'Header',
                'breadcrumbs' => 'Breadcrumbs',
                'resp-menu' => 'Responsive Menu',
                'footer' => 'Footer'
            ],
            'sections' => [
                'header' => 'Header',
                'navigation' => 'Navigation',
                'main' => 'Main',
                'footer' => 'Footer',
                'offcanvas' => 'Offcanvas'
            ],
            'particles' => [
                'logo' => [
                    'logo-8176' => 'Logo'
                ],
                'position' => [
                    'position-header' => 'Header',
                    'position-breadcrumbs' => 'Breadcrumbs',
                    'position-position-3999' => 'Responsive Menu',
                    'position-footer' => 'Footer'
                ],
                'messages' => [
                    'system-messages-5941' => 'System Messages'
                ],
                'content' => [
                    'system-content-9260' => 'Page Content'
                ]
            ],
            'inherit' => [
                'default' => [
                    'navigation' => 'navigation',
                    'footer' => 'footer',
                    'offcanvas' => 'offcanvas',
                    'position-position-3999' => 'position-position-9419',
                    'position-footer' => 'position-footer'
                ]
            ]
        ],
        'layout' => [
            'version' => 2,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/3-col.png',
                'name' => '_joomla_-_gantry4',
                'timestamp' => 1496913573
            ],
            'layout' => [
                '/drawer/' => [
                    
                ],
                '/top/' => [
                    0 => [
                        0 => 'position-position-4033'
                    ]
                ],
                '/header/' => [
                    0 => [
                        0 => 'position-position-4245'
                    ]
                ],
                '/slip/' => [
                    0 => [
                        0 => 'position-position-6311'
                    ]
                ],
                '/navigation/' => [
                    0 => [
                        0 => 'position-position-9419'
                    ]
                ],
                '/showcase/' => [
                    0 => [
                        0 => 'position-position-7473'
                    ]
                ],
                '/feature/' => [
                    
                ],
                '/utility/' => [
                    
                ],
                '/breadcrumb/' => [
                    
                ],
                '/maintop/' => [
                    
                ],
                '/container-main/' => [
                    0 => [
                        0 => [
                            'main-mainbody 80' => [
                                0 => [
                                    0 => 'system-content-6237'
                                ]
                            ]
                        ],
                        1 => [
                            'sidebar 20' => [
                                
                            ]
                        ]
                    ]
                ],
                '/gallery/' => [
                    0 => [
                        0 => 'position-position-5350'
                    ]
                ],
                '/mainbottom/' => [
                    0 => [
                        0 => 'position-position-1068'
                    ]
                ],
                '/extension/' => [
                    0 => [
                        0 => 'position-position-9973'
                    ]
                ],
                '/bottom/' => [
                    0 => [
                        0 => 'position-position-8707'
                    ]
                ],
                '/lowest/' => [
                    0 => [
                        0 => 'position-position-5886'
                    ]
                ],
                '/footer/' => [
                    0 => [
                        0 => 'position-footer'
                    ]
                ],
                '/copyright/' => [
                    0 => [
                        0 => 'position-position-2961'
                    ]
                ],
                'offcanvas' => [
                    
                ]
            ],
            'structure' => [
                'drawer' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'top' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'header' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'slip' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'navigation' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'showcase' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'feature' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'utility' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'breadcrumb' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'maintop' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'main-mainbody' => [
                    'title' => 'Mainbody'
                ],
                'sidebar' => [
                    'type' => 'section',
                    'subtype' => 'aside',
                    'block' => [
                        'fixed' => 1
                    ]
                ],
                'container-main' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'gallery' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'mainbottom' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'extension' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'bottom' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'lowest' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'footer' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'copyright' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ]
            ],
            'content' => [
                'position-position-4033' => [
                    'title' => 'Top',
                    'attributes' => [
                        'key' => 'top'
                    ],
                    'block' => [
                        'class' => 'top'
                    ]
                ],
                'position-position-4245' => [
                    'title' => 'Header',
                    'attributes' => [
                        'key' => 'header'
                    ],
                    'block' => [
                        'class' => 'header'
                    ]
                ],
                'position-position-6311' => [
                    'title' => 'Slip',
                    'attributes' => [
                        'key' => 'slip'
                    ],
                    'block' => [
                        'class' => 'slip'
                    ]
                ],
                'position-position-9419' => [
                    'title' => 'Responsive Menu',
                    'attributes' => [
                        'key' => 'resp-menu'
                    ],
                    'block' => [
                        'class' => 'responsive-menu'
                    ]
                ],
                'position-position-7473' => [
                    'title' => 'Slideshow',
                    'attributes' => [
                        'key' => 'slideshow'
                    ],
                    'block' => [
                        'class' => 'slideshow'
                    ]
                ],
                'position-position-5350' => [
                    'title' => 'Gallery',
                    'attributes' => [
                        'key' => 'gallery'
                    ],
                    'block' => [
                        'class' => 'gallery'
                    ]
                ],
                'position-position-1068' => [
                    'title' => 'Mainbottom',
                    'attributes' => [
                        'key' => 'mainbottom'
                    ],
                    'block' => [
                        'class' => 'main-bottom mainbottom'
                    ]
                ],
                'position-position-9973' => [
                    'title' => 'Extension',
                    'attributes' => [
                        'key' => 'extension'
                    ],
                    'block' => [
                        'class' => 'extension'
                    ]
                ],
                'position-position-8707' => [
                    'title' => 'Bottom',
                    'attributes' => [
                        'key' => 'bottom'
                    ],
                    'block' => [
                        'class' => 'bottom'
                    ]
                ],
                'position-position-5886' => [
                    'title' => 'Lowest',
                    'attributes' => [
                        'key' => 'lowest'
                    ],
                    'block' => [
                        'class' => 'lowest'
                    ]
                ],
                'position-footer' => [
                    'attributes' => [
                        'key' => 'footer'
                    ]
                ],
                'position-position-2961' => [
                    'title' => 'Copyright',
                    'attributes' => [
                        'key' => 'copyright'
                    ],
                    'block' => [
                        'class' => 'copy'
                    ]
                ]
            ]
        ],
        'layout2' => [
            'version' => 2,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'default',
                'timestamp' => 1496913581
            ],
            'layout' => [
                '/header/' => [
                    0 => [
                        0 => 'logo-8176 30',
                        1 => 'position-header 70'
                    ]
                ],
                'navigation' => [
                    
                ],
                '/main/' => [
                    0 => [
                        0 => 'position-breadcrumbs'
                    ],
                    1 => [
                        0 => 'system-messages-5941'
                    ],
                    2 => [
                        0 => 'system-content-9260'
                    ]
                ],
                'footer' => [
                    
                ],
                'offcanvas' => [
                    
                ]
            ],
            'structure' => [
                'header' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'navigation' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ]
                ],
                'main' => [
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'footer' => [
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ]
                ],
                'offcanvas' => [
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ]
                ]
            ],
            'content' => [
                'position-header' => [
                    'attributes' => [
                        'key' => 'header'
                    ]
                ],
                'position-breadcrumbs' => [
                    'attributes' => [
                        'key' => 'breadcrumbs'
                    ]
                ]
            ]
        ],
        'page' => [
            'assets' => [
                'javascript' => [
                    0 => [
                        'location' => '/bv/custom.js',
                        'inline' => '',
                        'in_footer' => '0',
                        'extra' => [
                            
                        ],
                        'priority' => '0',
                        'name' => 'Custom JS'
                    ]
                ]
            ],
            'head' => [
                'head_bottom' => '<meta name="viewport" content="width=1200px">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=1921998558057635";
  fjs.parentNode.insertBefore(js, fjs);
}(document, \'script\', \'facebook-jssdk\'));</script>'
            ]
        ]
    ]
];
