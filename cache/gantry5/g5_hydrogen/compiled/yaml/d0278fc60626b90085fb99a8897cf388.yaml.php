<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/home/sites/broadwayvets.co.uk/public_html/bv/templates/g5_hydrogen/custom/config/default/particles/date.yaml',
    'modified' => 1496936576,
    'data' => [
        'enabled' => '0',
        'css' => [
            'class' => 'date'
        ],
        'date' => [
            'formats' => 'l, F d, Y'
        ]
    ]
];
