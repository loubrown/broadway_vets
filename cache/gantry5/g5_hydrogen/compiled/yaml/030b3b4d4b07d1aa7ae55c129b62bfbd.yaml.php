<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/home/sites/broadwayvets.co.uk/public_html/bv/templates/g5_hydrogen/custom/config/default/particles/logo.yaml',
    'modified' => 1496936576,
    'data' => [
        'enabled' => '0',
        'url' => '',
        'image' => 'gantry-assets://images/gantry5-logo.png',
        'link' => '1',
        'svg' => '',
        'text' => 'Gantry 5',
        'class' => 'gantry-logo'
    ]
];
