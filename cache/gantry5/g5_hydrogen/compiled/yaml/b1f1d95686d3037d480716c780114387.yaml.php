<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/home/sites/broadwayvets.co.uk/public_html/bv/templates/g5_hydrogen/custom/config/default/styles.yaml',
    'modified' => 1496936924,
    'data' => [
        'preset' => 'preset1',
        'base' => [
            'background' => '#ffffff',
            'text-color' => '#666666',
            'body-font' => 'roboto, sans-serif',
            'heading-font' => 'roboto, sans-serif'
        ],
        'accent' => [
            'color-1' => '#439a86',
            'color-2' => '#8f4dae'
        ],
        'header' => [
            'background' => '#2a816d',
            'text-color' => '#ffffff'
        ],
        'navigation' => [
            'background' => '#439a86',
            'text-color' => '#ffffff',
            'overlay' => 'rgba(0, 0, 0, 0.4)'
        ],
        'showcase' => [
            'background' => '#354d59',
            'image' => '',
            'text-color' => '#ffffff'
        ],
        'feature' => [
            'background' => '#ffffff',
            'text-color' => '#666666'
        ],
        'subfeature' => [
            'background' => '#f0f0f0',
            'text-color' => '#666666'
        ],
        'main' => [
            'background' => '#ffffff',
            'text-color' => '#666666'
        ],
        'footer' => [
            'background' => '#ffffff',
            'text-color' => '#666666'
        ],
        'offcanvas' => [
            'background' => '#354d59',
            'text-color' => '#ffffff',
            'width' => '17rem',
            'toggle-color' => '#ffffff',
            'toggle-visibility' => '1'
        ],
        'breakpoints' => [
            'large-desktop-container' => '75rem',
            'desktop-container' => '60rem',
            'tablet-container' => '48rem',
            'large-mobile-container' => '30rem',
            'mobile-menu-breakpoint' => '48rem'
        ],
        'menu' => [
            'col-width' => '180px',
            'animation' => 'g-fade'
        ]
    ]
];
