<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/home/sites/broadwayvets.co.uk/public_html/bv/templates/g5_hydrogen/custom/particles/clientquotes.yaml',
    'modified' => 1497367985,
    'data' => [
        'name' => 'Client Quotes',
        'description' => 'Displays Sample Content',
        'type' => 'particle',
        'icon' => 'fa-pencil-square-o',
        'configuration' => [
            'caching' => [
                'type' => 'static'
            ]
        ],
        'form' => [
            'fields' => [
                'enabled' => [
                    'type' => 'input.checkbox',
                    'label' => 'Enabled',
                    'description' => 'Globally enable the particle.',
                    'default' => true
                ],
                'quotes' => [
                    'type' => 'collection.list',
                    'array' => true,
                    'label' => 'Client Quote',
                    'description' => 'Create a new client quote.',
                    'value' => 'title',
                    'ajax' => true,
                    'overridable' => false,
                    'fields' => [
                        '.title' => [
                            'type' => 'input.text',
                            'label' => 'Title'
                        ],
                        '.client' => [
                            'type' => 'input.text',
                            'label' => 'Client Name'
                        ],
                        '.location' => [
                            'type' => 'input.text',
                            'label' => 'Location'
                        ],
                        '.quote' => [
                            'type' => 'textarea.textarea',
                            'label' => 'Quote (max char:145)',
                            'maxlength' => 145
                        ]
                    ]
                ]
            ]
        ]
    ]
];
