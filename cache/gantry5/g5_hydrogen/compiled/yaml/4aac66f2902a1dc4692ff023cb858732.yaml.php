<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/home/sites/broadwayvets.co.uk/public_html/bv/templates/g5_hydrogen/custom/config/default/particles/contentarray.yaml',
    'modified' => 1496936576,
    'data' => [
        'enabled' => '1',
        'article' => [
            'display' => [
                'image' => [
                    'enabled' => 'intro'
                ],
                'text' => [
                    'type' => 'intro',
                    'limit' => '',
                    'formatting' => 'text'
                ],
                'title' => [
                    'enabled' => '',
                    'limit' => ''
                ],
                'date' => [
                    'enabled' => '',
                    'format' => 'l, F d, Y'
                ],
                'read_more' => [
                    'enabled' => 'show',
                    'label' => '',
                    'css' => ''
                ],
                'author' => [
                    'enabled' => 'show'
                ],
                'category' => [
                    'enabled' => 'link'
                ],
                'hits' => [
                    'enabled' => 'show'
                ]
            ]
        ],
        'css' => [
            'class' => ''
        ],
        'extra' => [
            
        ]
    ]
];
