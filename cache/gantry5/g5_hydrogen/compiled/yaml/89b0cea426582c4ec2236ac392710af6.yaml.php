<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:/wamp64/www/bv/templates/g5_hydrogen/custom/config/9/index.yaml',
    'modified' => 1498062615,
    'data' => [
        'name' => '9',
        'timestamp' => 1497276823,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/3-col.png',
            'name' => '_joomla_-_gantry4',
            'timestamp' => 1496913573
        ],
        'positions' => [
            'top' => 'Top',
            'header' => 'Header',
            'slip' => 'Slip',
            'resp-menu' => 'Responsive Menu',
            'slideshow' => 'Slideshow',
            'gallery' => 'Gallery',
            'mainbottom' => 'Mainbottom',
            'extension' => 'Extension',
            'bottom' => 'Bottom',
            'lowest' => 'Lowest',
            'footer' => 'Footer',
            'copyright' => 'Copyright'
        ],
        'sections' => [
            'drawer' => 'Drawer',
            'top' => 'Top',
            'navigation' => 'Navigation',
            'slip' => 'Slip',
            'showcase' => 'Showcase',
            'feature' => 'Feature',
            'utility' => 'Utility',
            'breadcrumb' => 'Breadcrumb',
            'maintop' => 'Maintop',
            'gallery' => 'Gallery',
            'mainbottom' => 'Mainbottom',
            'extension' => 'Extension',
            'bottom' => 'Bottom',
            'lowest' => 'Lowest',
            'copyright' => 'Copyright',
            'header' => 'Header',
            'main-mainbody' => 'Mainbody',
            'sidebar' => 'Sidebar',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'position' => [
                'position-position-4033' => 'Top',
                'position-position-4245' => 'Header',
                'position-position-6311' => 'Slip',
                'position-position-9419' => 'Responsive Menu',
                'position-position-7473' => 'Slideshow',
                'position-position-5350' => 'Gallery',
                'position-position-1068' => 'Mainbottom',
                'position-position-9973' => 'Extension',
                'position-position-8707' => 'Bottom',
                'position-position-5886' => 'Lowest',
                'position-footer' => 'Footer',
                'position-position-2961' => 'Copyright'
            ],
            'content' => [
                'system-content-6237' => 'Page Content'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
