<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/home/sites/broadwayvets.co.uk/public_html/bv/templates/g5_hydrogen/custom/config/9/index2.yaml',
    'modified' => 1496936266,
    'data' => [
        'name' => 9,
        'timestamp' => 1496936265,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'default',
            'timestamp' => 1496913581
        ],
        'positions' => [
            'header' => 'Header',
            'breadcrumbs' => 'Breadcrumbs',
            'resp-menu' => 'Responsive Menu',
            'footer' => 'Footer'
        ],
        'sections' => [
            'header' => 'Header',
            'navigation' => 'Navigation',
            'main' => 'Main',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'logo' => [
                'logo-8176' => 'Logo'
            ],
            'position' => [
                'position-header' => 'Header',
                'position-breadcrumbs' => 'Breadcrumbs',
                'position-position-3999' => 'Responsive Menu',
                'position-footer' => 'Footer'
            ],
            'messages' => [
                'system-messages-5941' => 'System Messages'
            ],
            'content' => [
                'system-content-9260' => 'Page Content'
            ]
        ],
        'inherit' => [
            'default' => [
                'navigation' => 'navigation',
                'footer' => 'footer',
                'offcanvas' => 'offcanvas',
                'position-position-3999' => 'position-position-9419',
                'position-footer' => 'position-footer'
            ]
        ]
    ]
];
