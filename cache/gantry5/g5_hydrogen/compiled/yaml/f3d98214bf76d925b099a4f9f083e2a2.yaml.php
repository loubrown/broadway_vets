<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/home/sites/broadwayvets.co.uk/public_html/bv/templates/g5_hydrogen/custom/config/default/page/assets.yaml',
    'modified' => 1496936607,
    'data' => [
        'favicon' => '',
        'touchicon' => '',
        'css' => [
            
        ],
        'javascript' => [
            0 => [
                'location' => '/bv/custom.js',
                'inline' => '',
                'in_footer' => '0',
                'extra' => [
                    
                ],
                'priority' => '0',
                'name' => 'Custom JS'
            ]
        ]
    ]
];
