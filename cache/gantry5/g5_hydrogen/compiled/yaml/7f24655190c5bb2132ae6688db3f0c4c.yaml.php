<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/home/sites/broadwayvets.co.uk/public_html/bv/templates/g5_hydrogen/custom/particles/newsletter.yaml',
    'modified' => 1497348040,
    'data' => [
        'name' => 'Newsletters',
        'description' => 'Displays Sample Content',
        'type' => 'particle',
        'icon' => 'fa-pencil-square-o',
        'configuration' => [
            'caching' => [
                'type' => 'static'
            ]
        ],
        'form' => [
            'fields' => [
                'enabled' => [
                    'type' => 'input.checkbox',
                    'label' => 'Enabled',
                    'description' => 'Globally enable the particle.',
                    'default' => true
                ],
                'image' => [
                    'type' => 'input.imagepicker',
                    'label' => 'Image',
                    'description' => 'Select the main image.',
                    'overridable' => false
                ],
                'headline' => [
                    'type' => 'input.text',
                    'label' => 'Headline',
                    'description' => 'Customize the headline text.',
                    'placeholder' => 'Enter headline',
                    'overridable' => false
                ],
                'description' => [
                    'type' => 'textarea.textarea',
                    'label' => 'Description',
                    'description' => 'Customize the description.',
                    'placeholder' => 'Enter short description',
                    'overridable' => false
                ],
                'link' => [
                    'type' => 'input.text',
                    'label' => 'Link',
                    'description' => 'Specify the link address.',
                    'overridable' => false
                ],
                'linktext' => [
                    'type' => 'input.text',
                    'label' => 'Link Text',
                    'description' => 'Customize the link text.',
                    'overridable' => false
                ],
                'newsletters' => [
                    'type' => 'collection.list',
                    'array' => true,
                    'label' => 'Newsletters',
                    'description' => 'Create a new newsletter download for users.',
                    'value' => 'title',
                    'ajax' => true,
                    'overridable' => false,
                    'fields' => [
                        '.title' => [
                            'type' => 'input.text',
                            'label' => 'Title'
                        ],
                        '.date' => [
                            'type' => 'input.date',
                            'label' => 'Date'
                        ],
                        '.newletter_file' => [
                            'type' => 'input.filepicker',
                            'label' => 'Newsletter File',
                            'description' => 'Please select the newsfile which you would like to upload.'
                        ],
                        '.intro' => [
                            'type' => 'textarea.textarea',
                            'label' => 'Newsletter Introduction'
                        ]
                    ]
                ]
            ]
        ]
    ]
];
