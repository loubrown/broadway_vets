<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/home/sites/broadwayvets.co.uk/public_html/bv/templates/g5_hydrogen/config/default/particles/logo.yaml',
    'modified' => 1496913581,
    'data' => [
        'enabled' => '1',
        'url' => '',
        'image' => 'gantry-assets://images/gantry5-logo.png',
        'text' => 'Gantry 5',
        'class' => 'gantry-logo'
    ]
];
