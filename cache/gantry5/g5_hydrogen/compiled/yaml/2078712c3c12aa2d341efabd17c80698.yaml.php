<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/home/sites/broadwayvets.co.uk/public_html/bv/templates/g5_hydrogen/custom/config/default/particles/branding.yaml',
    'modified' => 1496936576,
    'data' => [
        'enabled' => '0',
        'content' => 'Powered by <a href="http://www.gantry.org/" title="Gantry Framework" class="g-powered-by">Gantry Framework</a>',
        'css' => [
            'class' => 'branding'
        ]
    ]
];
