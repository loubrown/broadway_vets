<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/home/sites/broadwayvets.co.uk/public_html/bv/templates/g5_hydrogen/custom/config/default/layout.yaml',
    'modified' => 1496936266,
    'data' => [
        'version' => 2,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/3-col.png',
            'name' => '_joomla_-_gantry4',
            'timestamp' => 1496913573
        ],
        'layout' => [
            '/drawer/' => [
                
            ],
            '/top/' => [
                0 => [
                    0 => 'position-position-4033'
                ]
            ],
            '/header/' => [
                0 => [
                    0 => 'position-position-4245'
                ]
            ],
            '/slip/' => [
                0 => [
                    0 => 'position-position-6311'
                ]
            ],
            '/navigation/' => [
                0 => [
                    0 => 'position-position-9419'
                ]
            ],
            '/showcase/' => [
                0 => [
                    0 => 'position-position-7473'
                ]
            ],
            '/feature/' => [
                
            ],
            '/utility/' => [
                
            ],
            '/breadcrumb/' => [
                
            ],
            '/maintop/' => [
                
            ],
            '/container-main/' => [
                0 => [
                    0 => [
                        'main-mainbody 80' => [
                            0 => [
                                0 => 'system-content-6237'
                            ]
                        ]
                    ],
                    1 => [
                        'sidebar 20' => [
                            
                        ]
                    ]
                ]
            ],
            '/mainbottom/' => [
                0 => [
                    0 => 'position-position-1068'
                ]
            ],
            '/extension/' => [
                0 => [
                    0 => 'position-position-9973'
                ]
            ],
            '/bottom/' => [
                0 => [
                    0 => 'position-position-8707'
                ]
            ],
            '/lowest/' => [
                0 => [
                    0 => 'position-position-5886'
                ]
            ],
            '/footer/' => [
                0 => [
                    0 => 'position-footer'
                ]
            ],
            '/copyright/' => [
                0 => [
                    0 => 'position-position-2961'
                ]
            ],
            'offcanvas' => [
                
            ]
        ],
        'structure' => [
            'drawer' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'top' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'header' => [
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'slip' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'navigation' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'showcase' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'feature' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'utility' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'breadcrumb' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'maintop' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'main-mainbody' => [
                'title' => 'Mainbody'
            ],
            'sidebar' => [
                'type' => 'section',
                'subtype' => 'aside',
                'block' => [
                    'fixed' => 1
                ]
            ],
            'container-main' => [
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'mainbottom' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'extension' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'bottom' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'lowest' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'footer' => [
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'copyright' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ]
        ],
        'content' => [
            'position-position-4033' => [
                'title' => 'Top',
                'attributes' => [
                    'key' => 'top'
                ],
                'block' => [
                    'class' => 'top'
                ]
            ],
            'position-position-4245' => [
                'title' => 'Header',
                'attributes' => [
                    'key' => 'header'
                ],
                'block' => [
                    'class' => 'header'
                ]
            ],
            'position-position-6311' => [
                'title' => 'Slip',
                'attributes' => [
                    'key' => 'slip'
                ],
                'block' => [
                    'class' => 'slip'
                ]
            ],
            'position-position-9419' => [
                'title' => 'Responsive Menu',
                'attributes' => [
                    'key' => 'resp-menu'
                ],
                'block' => [
                    'class' => 'responsive-menu'
                ]
            ],
            'position-position-7473' => [
                'title' => 'Slideshow',
                'attributes' => [
                    'key' => 'slideshow'
                ],
                'block' => [
                    'class' => 'slideshow'
                ]
            ],
            'position-position-1068' => [
                'title' => 'Mainbottom',
                'attributes' => [
                    'key' => 'mainbottom'
                ],
                'block' => [
                    'class' => 'main-bottom mainbottom'
                ]
            ],
            'position-position-9973' => [
                'title' => 'Extension',
                'attributes' => [
                    'key' => 'extension'
                ],
                'block' => [
                    'class' => 'extension'
                ]
            ],
            'position-position-8707' => [
                'title' => 'Bottom',
                'attributes' => [
                    'key' => 'bottom'
                ],
                'block' => [
                    'class' => 'bottom'
                ]
            ],
            'position-position-5886' => [
                'title' => 'Lowest',
                'attributes' => [
                    'key' => 'lowest'
                ],
                'block' => [
                    'class' => 'lowest'
                ]
            ],
            'position-footer' => [
                'attributes' => [
                    'key' => 'footer'
                ]
            ],
            'position-position-2961' => [
                'title' => 'Copyright',
                'attributes' => [
                    'key' => 'copyright'
                ],
                'block' => [
                    'class' => 'copy'
                ]
            ]
        ]
    ]
];
