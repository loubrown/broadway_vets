<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:\\wamp64\\www\\bv/templates/g5_hydrogen/custom/config/default/particles/date.yaml',
    'modified' => 1498043369,
    'data' => [
        'enabled' => '0',
        'css' => [
            'class' => 'date'
        ],
        'date' => [
            'formats' => 'l, F d, Y'
        ]
    ]
];
