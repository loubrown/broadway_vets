<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:\\wamp64\\www\\bv/templates/g5_hydrogen/custom/config/default/particles/contentarray.yaml',
    'modified' => 1498043370,
    'data' => [
        'enabled' => '1',
        'article' => [
            'display' => [
                'image' => [
                    'enabled' => 'intro'
                ],
                'text' => [
                    'type' => 'intro',
                    'limit' => '',
                    'formatting' => 'text'
                ],
                'title' => [
                    'enabled' => '',
                    'limit' => ''
                ],
                'date' => [
                    'enabled' => '',
                    'format' => 'l, F d, Y'
                ],
                'read_more' => [
                    'enabled' => 'show',
                    'label' => '',
                    'css' => ''
                ],
                'author' => [
                    'enabled' => 'show'
                ],
                'category' => [
                    'enabled' => 'link'
                ],
                'hits' => [
                    'enabled' => 'show'
                ]
            ]
        ],
        'css' => [
            'class' => ''
        ],
        'extra' => [
            
        ]
    ]
];
