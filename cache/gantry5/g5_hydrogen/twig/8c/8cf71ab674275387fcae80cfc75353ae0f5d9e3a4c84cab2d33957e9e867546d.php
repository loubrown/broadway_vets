<?php

/* @particles/clientquotes.html.twig */
class __TwigTemplate_63c053009bdb56a3e61787cb8925e0ac97dbdb32a561060448e11afd0b4409d7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@nucleus/partials/particle.html.twig", "@particles/clientquotes.html.twig", 1);
        $this->blocks = array(
            'particle' => array($this, 'block_particle'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@nucleus/partials/particle.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_particle($context, array $blocks = array())
    {
        // line 4
        echo "\t<div class=\"client-quotes\">
\t";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["particle"]) ? $context["particle"] : null), "quotes", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["quote"]) {
            // line 6
            echo "\t\t<p>\"";
            echo $this->getAttribute($context["quote"], "quote", array());
            echo "\" - <span class=\"yell\">";
            echo $this->getAttribute($context["quote"], "client", array());
            echo ", ";
            echo $this->getAttribute($context["quote"], "location", array());
            echo "</span></p>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['quote'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo "\t</div>
";
    }

    public function getTemplateName()
    {
        return "@particles/clientquotes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 8,  38 => 6,  34 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@particles/clientquotes.html.twig", "/home/sites/broadwayvets.co.uk/public_html/bv/templates/g5_hydrogen/custom/particles/clientquotes.html.twig");
    }
}
