<?php

/* @particles/newsletter.html.twig */
class __TwigTemplate_a60976203728fab541c75a3ebbc0b8abb9ab7bf47f184e069b63d85402fe1f3c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@nucleus/partials/particle.html.twig", "@particles/newsletter.html.twig", 1);
        $this->blocks = array(
            'particle' => array($this, 'block_particle'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@nucleus/partials/particle.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_particle($context, array $blocks = array())
    {
        // line 4
        echo "\t<div class=\"newsletter-content\">
\t\t<div class=\"g-grid\">
\t\t\t";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_reverse_filter($this->env, $this->getAttribute((isset($context["particle"]) ? $context["particle"] : null), "newsletters", array())));
        foreach ($context['_seq'] as $context["_key"] => $context["newsletter"]) {
            // line 7
            echo "\t\t\t\t<div class=\"newsletter\">
\t\t\t\t\t<h4>";
            // line 8
            echo $this->getAttribute($context["newsletter"], "title", array());
            echo "</h4>
\t\t\t\t\t";
            // line 9
            $context["aa"] = call_user_func_array($this->env->getFilter('date')->getCallable(), array($this->env, $this->getAttribute($context["newsletter"], "date", array()), "d"));
            // line 10
            echo "\t\t\t\t\t";
            $context["ab"] = call_user_func_array($this->env->getFilter('date')->getCallable(), array($this->env, $this->getAttribute($context["newsletter"], "date", array()), "S"));
            // line 11
            echo "                    ";
            $context["ac"] = call_user_func_array($this->env->getFilter('date')->getCallable(), array($this->env, $this->getAttribute($context["newsletter"], "date", array()), "F Y"));
            // line 12
            echo "\t\t\t\t\t<p class=\"date\">";
            echo twig_escape_filter($this->env, (isset($context["aa"]) ? $context["aa"] : null), "html", null, true);
            echo "<sup>";
            echo twig_escape_filter($this->env, (isset($context["ab"]) ? $context["ab"] : null), "html", null, true);
            echo "</sup> ";
            echo twig_escape_filter($this->env, (isset($context["ac"]) ? $context["ac"] : null), "html", null, true);
            echo "</p>
\t\t\t\t\t<p>";
            // line 13
            echo nl2br(twig_escape_filter($this->env, $this->getAttribute($context["newsletter"], "intro", array()), "html", null, true));
            echo "</p>
\t\t\t\t\t<a href=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->urlFunc($this->getAttribute($context["newsletter"], "newletter_file", array())), "html", null, true);
            echo "\" target=\"_blank\" class=\"news-link\"></a>
\t\t\t\t</div>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['newsletter'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 17
        echo "\t\t</div>
\t</div>
";
    }

    public function getTemplateName()
    {
        return "@particles/newsletter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 17,  67 => 14,  63 => 13,  54 => 12,  51 => 11,  48 => 10,  46 => 9,  42 => 8,  39 => 7,  35 => 6,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@particles/newsletter.html.twig", "/home/sites/broadwayvets.co.uk/public_html/bv/templates/g5_hydrogen/custom/particles/newsletter.html.twig");
    }
}
