<?php

/* @gantry-admin/partials/php_unsupported.html.twig */
class __TwigTemplate_b179df623c2adce28c6536c6e2bd608522fccc00d708bf7a55d8630cd0a56e92 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["php_version"] = twig_constant("PHP_VERSION");
        // line 2
        echo "
";
        // line 3
        if ((is_string($__internal_1a026d727bc092ca70bad13d4a422019bda5e16830208d58e7d2ad3740329a3a = (isset($context["php_version"]) ? $context["php_version"] : null)) && is_string($__internal_0526eae488daa7aed95cd897f52b9b54cb87929cbab0c755a7d8bcac5ae1e492 = "5.4") && ('' === $__internal_0526eae488daa7aed95cd897f52b9b54cb87929cbab0c755a7d8bcac5ae1e492 || 0 === strpos($__internal_1a026d727bc092ca70bad13d4a422019bda5e16830208d58e7d2ad3740329a3a, $__internal_0526eae488daa7aed95cd897f52b9b54cb87929cbab0c755a7d8bcac5ae1e492)))) {
            // line 4
            echo "<div class=\"g-grid\">
    <div class=\"g-block alert alert-warning g-php-outdated\">
        ";
            // line 6
            echo $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->transFilter("GANTRY5_PLATFORM_PHP54_WARNING", (isset($context["php_version"]) ? $context["php_version"] : null));
            echo "
    </div>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "@gantry-admin/partials/php_unsupported.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 6,  26 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@gantry-admin/partials/php_unsupported.html.twig", "C:\\wamp64\\www\\bv\\administrator\\components\\com_gantry5\\templates\\partials\\php_unsupported.html.twig");
    }
}
