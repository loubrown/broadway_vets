j = jQuery.noConflict();
w = 0;

j(document).ready(function(){
    pageClass()
    dropdown()
    tabber()
    bannerTitle()
});

j(window).load(function(){
    quote()
});

j(window).resize(function(){

});

j(window).scroll(function(){

});

function pageClass(){
    var c = j('head title').text().toLowerCase().replace(/Broadway Vets - /i, '').replace(/\s/g, '-').replace(/-+/g, '-');
    j('body').addClass(c);
}

function dropdown() {
    jQuery('.menumain-menu > li.parent.deeper').mouseenter(function () {
        if (jQuery(window).width() > 750) {
            jQuery(this).find('> ul').stop().slideToggle(400);
        }
    });
    jQuery('.menumain-menu > li.parent.deeper').mouseleave(function () {
        if (jQuery(window).width() > 750) {
            jQuery(this).find('> ul').stop().slideToggle(400);
        }
    });
}

function tabber(){
    j('.menuresp-menu > li.parent.deeper').each(function(){
        j(this).find('> a').after('<span class="drop"></span>');
    });

    j('.tabber').click(function(){
        j('.menuresp-menu').slideToggle(700);
        j('.menuresp-menu li > ul').slideUp(400);
    });

    j('.drop').click(function(){
        if(j(this).hasClass('active')){
            j(this).removeClass('active');
        } else{
            j(this).addClass('active')
        }
        j(this).next('ul').slideToggle(700);
    });
}

function bannerTitle(){
    var tit = j('.item-page > div > h2').eq(0).text();
    j('.banner h2').text(tit);
}

function quote(){
    p = [];
    j('.client-quotes p').each(function(){
        w += j(this).outerWidth()
        p.push(j(this).html())
    });
    for(i=0; i<p.length;i++){
        j('.client-quotes').append('<p>' + p[i] + '</p>')
    }
    j('.client-quotes').width(w*2)
    carousel()
}

function carousel(){
    j('.client-quotes').animate({'left': -w + 'px'}, 45000, 'linear', function(){
        j('.client-quotes').css('left', 0)
        carousel()
    })
}